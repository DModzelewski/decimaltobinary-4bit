package MainPackage;

import MainPackage.Message.Error;
import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        System.out.println("Przelicznik:");
        Scanner input = new Scanner(System.in);
        Error error = new Error();

        boolean running = true; // sprawdzenie czy program działa
        int bytesNumber = 4; // liczba bitów
        while (running) {
            System.out.println("Podaj liczbę dodatnią od 0 do 15");
            int decimalNumber = input.nextInt();
            if (decimalNumber > 15 || decimalNumber < 0) {
               error.ThrowError();
            } else {
                System.out.println("Twoja liczba w systemie dziesiętnym: " + decimalNumber);
                int[] binaryNumber = new int[bytesNumber];
                // tu zaczynamy właściwe liczenie:
                int i; //iterator
                for (i = (bytesNumber-1); i >= 0; i--) {
                        binaryNumber[i] = decimalNumber % 2;
                        decimalNumber = decimalNumber / 2;
                }
                System.out.print("Twoja liczba binarnie to: ");
                for (int value : binaryNumber) {
                    System.out.print(value);
                }
                System.out.println("");
                System.out.println("1 - kolejna liczba \n0 - koniec");
                int command = input.nextInt();
                if (command == 0) {
                    running = false;
                } else if (command != 1) {
                    System.out.println("Błąd. Za karę zamykam program");
                    running = false;
                }
            }
        }
    }
}
